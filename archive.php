<?php 
/*----------------------------------------------------------------*\

	DEFAULT CATEGORY ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<header class="post-head is-wide">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="is-narrow">
				<?php	while ( have_posts() ) : the_post(); ?>
					<article class="archive-result <?php echo $post_type; ?>">
						<header>
							<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
						</header>
						<div class="entry-content">
							<?php the_excerpt(); ?>
						</div>
					</article>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<h2>Coming soon</h2>
				</section>
			</article>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_footer(); ?>