<?php 
/*----------------------------------------------------------------*\

	SINGLE POST FOR CPT: ISSUE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main id="main-content">
	<article>
	<?php if (have_posts()) : ?>
		<?php	while ( have_posts() ) : the_post(); ?>
			<section class="issue is-extra-wide">
				<div>
					<?php foreach ( get_the_terms(get_the_ID(), 'series') as $cat ) : ?>
					<h6><?php echo $cat->name ?> - Issue <?php the_field('issue_number'); ?></h6>
					<?php endforeach; ?>
					<h1><?php the_title(); ?></h1>
					<?php the_field('description'); ?>
					<img class="sgl-rating" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/SGL-Rating.svg" alt="Super Geek League Rating" />
					<button class="activate-share-menu">
						<svg>
							<use xlink:href="#share" />
						</svg>
						Share
					</button>
				</div>
				<div class="video-wrap">
					<?php $poster = get_field('issue_image'); ?>
					<?php $posterurl = $poster['url']; ?>
					<?php $posteralt = $poster['alt']; ?>

					<?php $video = get_field('video'); ?>
					<?php $videourl = $video['url']; ?>

					<?php $issuenum = get_field('issue_number'); ?>
					<?php $nextnum = $issuenum + 1; ?>
					<?php if( $nextnum < 10 ): ?>
					<?php $nextissuenum = '0' . $nextnum; ?>
					<?php else : ?>
					<?php $nextissuenum = $nextnum; ?>
					<?php endif; ?>

					<?php $issueproduct = get_field('issue_product_link'); ?>
					<?php $issueproductid = $issueproduct[0]->ID; ?>
					<?php $issueproductslug = $issueproduct[0]->slug; ?>
					<?php $issuewooproduct = new WC_Product($issueproductid); ?>
					<?php $issueslug = $issuewooproduct->get_slug(); ?>
					<?php $issueprice = wc_price($issuewooproduct->get_price_excluding_tax(1,$issuewooproduct->get_price())); ?>

					<?php $seriesproduct = get_field('series_product_link'); ?>
					<?php $seriesproductid = $seriesproduct[0]->ID; ?>
					<?php $seriesproducttitle = $seriesproduct[0]->post_title; ?>
					<?php $serieswooproduct = new WC_Product($seriesproductid); ?>
					<?php $seriesslug = $serieswooproduct->get_slug(); ?>
					<?php $seriesprice = wc_price($serieswooproduct->get_price_excluding_tax(1,$serieswooproduct->get_price())); ?>

					<?php $nextproduct = get_field('next_issue_link'); ?>
					<?php $nextproductid = $nextproduct[0]->ID; ?>
					<?php $nextproductslug = $nextproduct[0]->slug; ?>
					<?php $nextwooproduct = new WC_Product($nextproductid); ?>
					<?php $nextslug = $nextwooproduct->get_slug(); ?>
					<?php $nextprice = wc_price($nextwooproduct->get_price_excluding_tax(1,$nextwooproduct->get_price())); ?>
					
					<?php if ( get_field('free_to_watch') ): ?>
						<?php if ( is_user_logged_in() ) : ?>
							<?php echo '<video controls playsinline poster="' . $posterurl . '"><source src="' . $videourl . '" type="video/mp4">Your browser does not support HTML videos.</video><button id="skip-intro">Skip Intro</button>'; ?>
							<?php echo do_shortcode('[wcm_nonmember plans="' . $seriesslug . '"]<div class="series-upsell"><h4>Like this issue?</h4><a class="button" href="/cart/?add-to-cart=' . $seriesproductid . '">Unlock ' . $seriesproducttitle . ' Pass - ' . $seriesprice . '</a></div>[/wcm_nonmember]'); ?>
						<?php else : ?>
							<?php echo '<img src="' . $posterurl . '" alt="' . $posteralt . '"><div class="video-overlay"><a class="button is-pink" href="/my-account/">Sign up to watch free</a></div>'; ?>
						<?php endif; ?>
					
					<?php else : ?>
						<?php 
							$upsell = do_shortcode('[wcm_nonmember plans="' . $seriesslug . '"]<div class="series-upsell"><h4>Like this issue?</h4><a class="button" href="/cart/?add-to-cart=' . $seriesproductid . '">Unlock ' . $seriesproducttitle . ' Pass - ' . $seriesprice . '</a></div>[/wcm_nonmember]');
							$upsellclass = do_shortcode('[wcm_nonmember plans="' . $seriesslug . '"]class="upsell"[/wcm_nonmember]');
						?>
						<?php echo do_shortcode('[wcm_nonmember plans="' . $issueslug . ', ' . $seriesslug . '"]<img src="' . $posterurl . '" alt="' . $posteralt . '"><div class="video-overlay"><h3>Watch Issue ' . $issuenum . ' Now:</h3><a class="button is-pink" href="/cart/?add-to-cart=' . $issueproductid . '">Unlock Issue ' . $issuenum . ' - ' . $issueprice . '</a><p>Or unlock every issue of ' . $seriesproducttitle . ':</p><a class="button" href="/cart/?add-to-cart=' . $seriesproductid . '">Unlock ' . $seriesproducttitle . ' Pass - ' . $seriesprice . '</a></div>[/wcm_nonmember]'); ?>
						<?php echo do_shortcode('[wcm_restrict plans="' . $issueslug . ', ' . $seriesslug . '"]<video controls playsinline poster="' . $posterurl . '"><source src="' . $videourl . '" type="video/mp4">Your browser does not support HTML videos.</video><button id="skip-intro" ' . $upsellclass . '>Skip Intro</button>' . $upsell . '[/wcm_restrict]'); ?>
					<?php endif; ?>

					<?php if ( get_field('next_issue_link') ): ?>
					<div class="end-overlay">
						<h3>Keep watching?</h3>
						<?php echo do_shortcode('[wcm_nonmember plans="' . $nextslug . ', ' . $seriesslug . '"]<a class="button is-pink" href="/cart/?add-to-cart=' . $nextproductid . '">Unlock Issue ' . $nextissuenum . ' - ' . $nextprice . '</a>[/wcm_nonmember]'); ?>
						<?php echo do_shortcode('[wcm_restrict plans="' . $nextslug . ', ' . $seriesslug . '"]<a class="button is-pink" href="/issue/' . $nextslug . '">Next Issue</a>[/wcm_restrict]'); ?>
					</div>
					<?php endif; ?>
				</div>
			</section>

			<?php if ( get_field('free_to_watch') ): ?>

			<?php 
				ob_start( );
				get_template_part('template-parts/sections/series-callout');
				$output = ob_get_clean();
			?>

			<?php echo do_shortcode('[wcm_nonmember plans="series-01"]' . $output . '[/wcm_nonmember]'); ?>

			<?php endif; ?>

			<?php get_template_part('template-parts/sections/related-issues'); ?>

		<?php endwhile; ?>
	<?php else : ?>
		<section class="is-narrow">
			<h2>Coming soon</h2>
		</section>
	<?php endif; ?>
	<?php wp_reset_postdata(); ?>
	</article>
</main>

<?php get_footer(); ?>