<?php
/*----------------------------------------------------------------*\
	INITIALIZE POST TYPES
\*----------------------------------------------------------------*/
// Register Custom Post Type Issue
function create_issue_cpt() {

	$labels = array(
		'name' => _x( 'Issues', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Issue', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Issues', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Issue', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Issue Archives', 'textdomain' ),
		'attributes' => __( 'Issue Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Issue:', 'textdomain' ),
		'all_items' => __( 'All Issues', 'textdomain' ),
		'add_new_item' => __( 'Add New Issue', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Issue', 'textdomain' ),
		'edit_item' => __( 'Edit Issue', 'textdomain' ),
		'update_item' => __( 'Update Issue', 'textdomain' ),
		'view_item' => __( 'View Issue', 'textdomain' ),
		'view_items' => __( 'View Issues', 'textdomain' ),
		'search_items' => __( 'Search Issue', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Issue', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Issue', 'textdomain' ),
		'items_list' => __( 'Issues list', 'textdomain' ),
		'items_list_navigation' => __( 'Issues list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Issues list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Issue', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-video',
		'supports' => array('title', 'editor', 'excerpt', 'post-formats', 'custom-fields'),
		'taxonomies' => array('series', 'issue'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'issue', $args );

}
add_action( 'init', 'create_issue_cpt', 0 );

// Register Custom Post Type Bonkapedia
function create_bonkapedia_cpt() {

	$labels = array(
		'name' => _x( 'Bonkapedia', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Bonkapedia', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Bonkapedia', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Bonkapedia', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Bonkapedia Archives', 'textdomain' ),
		'attributes' => __( 'Bonkapedia Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Bonkapedia:', 'textdomain' ),
		'all_items' => __( 'All Bonkapedia', 'textdomain' ),
		'add_new_item' => __( 'Add New Bonkapedia', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Bonkapedia', 'textdomain' ),
		'edit_item' => __( 'Edit Bonkapedia', 'textdomain' ),
		'update_item' => __( 'Update Bonkapedia', 'textdomain' ),
		'view_item' => __( 'View Bonkapedia', 'textdomain' ),
		'view_items' => __( 'View Bonkapedia', 'textdomain' ),
		'search_items' => __( 'Search Bonkapedia', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Bonkapedia', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Bonkapedia', 'textdomain' ),
		'items_list' => __( 'Bonkapedia list', 'textdomain' ),
		'items_list_navigation' => __( 'Bonkapedia list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Bonkapedia list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Bonkapedia', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-book-alt',
		'supports' => array('title', 'editor', 'excerpt', 'post-formats', 'custom-fields'),
		'taxonomies' => array('bonk_category', 'bonk_tag'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'bonkapedia', $args );

}
add_action( 'init', 'create_bonkapedia_cpt', 0 );