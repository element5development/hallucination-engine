<?php
/*----------------------------------------------------------------*\
	GAVITY FORM TAB INDEX FIX
\*----------------------------------------------------------------*/
add_filter("gform_tabindex", create_function("", "return false;"));

/*----------------------------------------------------------------*\
	ENABLE LABEL VISABILITY
\*----------------------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*----------------------------------------------------------------*\
	CHANGE SUBMIT INPUTS TO BUTTONS
\*----------------------------------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );

/*----------------------------------------------------------------*\
	CONFIRMATION ANCHOR
\*----------------------------------------------------------------*/
add_filter( 'gform_confirmation_anchor', '__return_false' );