<?php
/*----------------------------------------------------------------*\
	INITIALIZE TAXONOMIES
\*----------------------------------------------------------------*/
// Register Taxonomy Category
function create_category_tax() {

	$labels = array(
		'name'              => _x( 'Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Categories', 'textdomain' ),
		'all_items'         => __( 'All Categories', 'textdomain' ),
		'parent_item'       => __( 'Parent Category', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Category:', 'textdomain' ),
		'edit_item'         => __( 'Edit Category', 'textdomain' ),
		'update_item'       => __( 'Update Category', 'textdomain' ),
		'add_new_item'      => __( 'Add New Category', 'textdomain' ),
		'new_item_name'     => __( 'New Category Name', 'textdomain' ),
		'menu_name'         => __( 'Category', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'bonk_category', array('bonkapedia'), $args );

}
add_action( 'init', 'create_category_tax' );

// Register Taxonomy Tag
function create_tag_tax() {

	$labels = array(
		'name'              => _x( 'Tags', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Tag', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Tags', 'textdomain' ),
		'all_items'         => __( 'All Tags', 'textdomain' ),
		'parent_item'       => __( 'Parent Tag', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Tag:', 'textdomain' ),
		'edit_item'         => __( 'Edit Tag', 'textdomain' ),
		'update_item'       => __( 'Update Tag', 'textdomain' ),
		'add_new_item'      => __( 'Add New Tag', 'textdomain' ),
		'new_item_name'     => __( 'New Tag Name', 'textdomain' ),
		'menu_name'         => __( 'Tag', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => false,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'bonk_tag', array('bonkapedia'), $args );

}
add_action( 'init', 'create_tag_tax' );

// Register Taxonomy Series
function create_series_tax() {

	$labels = array(
		'name'              => _x( 'Series', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Series', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Series', 'textdomain' ),
		'all_items'         => __( 'All Series', 'textdomain' ),
		'parent_item'       => __( 'Parent Series', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Series:', 'textdomain' ),
		'edit_item'         => __( 'Edit Series', 'textdomain' ),
		'update_item'       => __( 'Update Series', 'textdomain' ),
		'add_new_item'      => __( 'Add New Series', 'textdomain' ),
		'new_item_name'     => __( 'New Series Name', 'textdomain' ),
		'menu_name'         => __( 'Series', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
		'show_in_rest' => true,
	);
	register_taxonomy( 'series', array('issue'), $args );

}
add_action( 'init', 'create_series_tax' );