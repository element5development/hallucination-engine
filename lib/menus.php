<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_navigation' => __( 'Primary Menu' ),
		'secondary_navigation' => __( 'Secondary Menu' ),
		'sticky_navigation' => __( 'Sticky Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );