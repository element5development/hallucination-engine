<?php
/*----------------------------------------------------------------*\
		INITIALIZE WOOCOMMERCE
\*----------------------------------------------------------------*/
function theme_woocommerce_support() {
	add_theme_support( 'woocommerce', array(
		'single_image_width'    => 600,
			'product_grid'          => array(
					'default_rows'    => 4,
					'min_rows'        => 2,
					'max_rows'        => 8,
					'default_columns' => 3,
					'min_columns'     => 2,
					'max_columns'     => 5,
			),
	) );
}
add_action( 'after_setup_theme', 'theme_woocommerce_support' );
/*----------------------------------------------------------------*\
		DISABLE BREADCRUMBS
\*----------------------------------------------------------------*/
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
/*----------------------------------------------------------------*\
		REMOVE SORT BY
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
/*----------------------------------------------------------------*\
		REMOVE PRODUCT IMAGE LINKS
\*----------------------------------------------------------------*/
function remove_product_image_link( $html, $post_id ) {
	return preg_replace( "!<(a|/a).*?>!", '', $html );
}
add_filter( 'woocommerce_single_product_image_thumbnail_html', 'remove_product_image_link', 10, 2 );
/*----------------------------------------------------------------*\
		REMOVE RESULTS COUNT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
/*----------------------------------------------------------------*\
		REMOVE SIDEBAR
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
/*----------------------------------------------------------------*\
		SALE TEXT CHANGE
\*----------------------------------------------------------------*/
function vs_change_sale_content($content, $post, $product){
  $content = '<span class="sale">'.__( 'On Sale', 'woocommerce' ).'</span>';
  return $content;
}
add_filter('woocommerce_sale_flash', 'vs_change_sale_content', 10, 3);
/*----------------------------------------------------------------*\
		REMOVE STAR RATING BELOW TITLE
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
/*----------------------------------------------------------------*\
		NUMBER OF PRODUCTS PER PAGE
\*----------------------------------------------------------------*/
function new_loop_shop_per_page( $cols ) {
  $cols = 12;
  return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
/*----------------------------------------------------------------*\
		REMOVE SHORT DESCRIPTION
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
/*----------------------------------------------------------------*\
		REMOVE PRODUCT META
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
/*----------------------------------------------------------------*\
		REMOVE PRODUCT UPSELL AND RELATED PRODUCTS
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
/*----------------------------------------------------------------*\
		REMOVE TABS
\*----------------------------------------------------------------*/
function woo_remove_product_tabs( $tabs ) {
	unset( $tabs['description'] );          
	unset( $tabs['reviews'] );          
	unset( $tabs['additional_information'] );   
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
/*----------------------------------------------------------------*\
		RELOCATE DESCRIPTION
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'the_content', 20 );
/*----------------------------------------------------------------*\
		RELOCATE DESCRIPTION
\*----------------------------------------------------------------*/
add_filter('woocommerce_cart_item_remove_link','remove_cart_icon',10,2);
	function remove_cart_icon($plink,$link) {
		return '';
	}
add_filter('woocommerce_cart_item_quantity','change_cart_remove_link',10,3);
function change_cart_remove_link($product_quantity, $cart_item_key, $cart_item) {
    //create new product object
    $_product = new WC_Product($cart_item['product_id']);//product_id
    $cart_remove_link = sprintf(
			'<a href="%s" class="remove button button_js" aria-label="%s" data-product_id="%s" data-product_sku="%s"><span class="button_icon">&times;</span></a>',
			esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
			__( 'Remove this item', 'woocommerce' ),
			esc_attr( $_product->get_id() ),
			esc_attr( $_product->get_sku() )
		);
    $product_quantity .= $cart_remove_link; 
    return $product_quantity;
}
/*----------------------------------------------------------------*\
		SHIPPING ADDRESS ENABLED BY DEFAULT
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
function shipchange( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
	case 'Ship to a different address?' :
		$translated_text = __( 'Ship to a different address', 'woocommerce' );
		break;
	}
	return $translated_text;
}
add_filter('gettext', 'shipchange', 20, 3);
/*----------------------------------------------------------------*\
		SHIPPING ADDRESS HEADLINE
\*----------------------------------------------------------------*/
add_filter('gettext', 'translate_reply');
add_filter('ngettext', 'translate_reply');
function translate_reply($translated) {
	$translated = str_ireplace('Ship to a different address?', 'Shipping Address', $translated);
	return $translated;
}
/*----------------------------------------------------------------*\
		AUTO CHECK DIFFERENT ADDRESS
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
/*----------------------------------------------------------------*\
		REMOVE ISSUE AND SERIES CATEGORIES FROM SHOP PAGE
\*----------------------------------------------------------------*/
function custom_pre_get_posts_query( $q ) {
	$tax_query = (array) $q->get( 'tax_query' );
	$tax_query[] = array(
				 'taxonomy' => 'product_cat',
				 'field' => 'slug',
				 'terms' => array( 'issue', 'series' ), // Don't display products in these categories on the shop page.
				 'operator' => 'NOT IN'
	);
	$q->set( 'tax_query', $tax_query );
}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' ); 
/*----------------------------------------------------------------*\
		REMOVE LINKS FROM CART PAGE
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_cart_item_permalink', '__return_null' );
/*----------------------------------------------------------------*\
		ADD QUANTITY LABEL TO PRODUCT PAGE
\*----------------------------------------------------------------*/
add_action( 'woocommerce_before_add_to_cart_quantity', 'bbloomer_echo_qty_front_add_cart' );
 
function bbloomer_echo_qty_front_add_cart() {
 echo '<div class="qty">Quantity: </div>'; 
}
/*----------------------------------------------------------------*\
		EDIT MY ACCOUNT MENU
\*----------------------------------------------------------------*/
// Add Issues
add_filter ( 'woocommerce_account_menu_items', 'additional_links' );
function additional_links( $menu_links ){
	$new = array( 'issues' => 'Issues' );
	// array_slice() is good when you want to add an element between the other ones
	$menu_links = array_slice( $menu_links, 0, 2, true ) 
	+ $new 
	+ array_slice( $menu_links, 1, NULL, true );
	return $menu_links;
}
add_filter( 'woocommerce_get_endpoint_url', 'additional_hooks', 10, 4 );
function additional_hooks( $url, $endpoint, $value, $permalink ){
	if( $endpoint === 'issues' ) {
		$url = '/issue/';
	}
	return $url;
}
// Edit Menu Order
function my_account_menu_order() {
	$menuOrder = array(
		'dashboard'           => __( 'Dashboard', 'woocommerce' ),
		'issues'          		=> __( 'Issues', 'woocommerce' ),
		'points-and-rewards'  => __( 'Bonk Points', 'woocommerce' ),
		'orders'              => __( 'Orders', 'woocommerce' ),
		'edit-address'        => __( 'Addresses', 'woocommerce' ),
		'edit-account'    		=> __( 'Account Details', 'woocommerce' ),
		'customer-logout'    	=> __( 'Logout', 'woocommerce' ),
	);
	return $menuOrder;
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );
/*----------------------------------------------------------------*\
		ADD POINTS SHORTCODE
\*----------------------------------------------------------------*/
add_shortcode ('points_shortcode', 'woocommerce_points_rewards_my_points');
/*----------------------------------------------------------------*\
		UPDATE THANK YOU MESSAGE
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_thankyou_order_received_text', 'thank_you' );
function thank_you() {
 $added_text = '<h3>Thanks for your order! You’re OK in our book, despite what we saw on Reddit. We don’t believe a word of it. Really. You’re bonkalicious.</h3>';
 return $added_text ;
}
/*----------------------------------------------------------------*\
		NEW CUSTOMER ACCOUNT SIGN UP
\*----------------------------------------------------------------*/
add_action( 'woocommerce_created_customer', 'woocommerce_created_customer_admin_notification' );
function woocommerce_created_customer_admin_notification( $customer_id ) {
  wp_send_new_user_notifications( $customer_id, 'admin' );
}
/*----------------------------------------------------------------*\
		NEW CUSTOMER REGISTER REDIRECT
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_registration_redirect', 'custom_redirection_after_registration', 10, 1 );
function custom_redirection_after_registration( $redirection_url ){
	// Change the redirection Url
	$redirection_url = get_permalink( wc_get_page_id( 'myaccount' ) ); // My Account

	return $redirection_url; // Always return something
}
/*----------------------------------------------------------------*\
		CHANGE RETURN TO SHOP LINK
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_return_to_shop_redirect', 'store_wc_empty_cart_redirect_url' );
function store_wc_empty_cart_redirect_url() {
	$url = '/issue/'; // change this link to your need
	return esc_url( $url );
}