<?php
/*----------------------------------------------------------------*\
	INITIALIZE BUTTON SHORTCODE
\*----------------------------------------------------------------*/
function button_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'size' => 'normal',
			'color' => 'black',
			'fill' => 'no',
			'target' => '_self',
			'url' => '#',
		),
		$atts,
		'button'
	);
	$size = $atts['size'];
	$color = $atts['color'];
	$fill = $atts['fill'];
	$target = $atts['target'];
	$url = $atts['url'];

	$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$color.' is-'.$fill.' is-'.$size.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode( 'button', 'button_shortcode' );