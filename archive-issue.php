<?php 
/*----------------------------------------------------------------*\

	ARCHIVE FOR CPT: ISSUE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<header class="post-head is-extra-wide">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>

			<?php
				$args = array (
					'orderby'    => 'name',
					'order'      => 'ASC',
				);

				$terms = get_terms( 'series', $args );
			?>
			<?php foreach ( $terms as $term ) { ?>

			<section class="issue-grid is-extra-wide">
				<h2><?php echo $term->name ?></h2>
				<?php 
					$post_args = array ( 
						'posts_per_page'  => -1, 
						'post_type' 	=> 'issue',
						'series'			=> $term->name,
					);
					$issue_query = new WP_Query( $post_args );
				?>
				<?php if ( $issue_query->have_posts() ) : ?>
					<?php while ( $issue_query->have_posts() ) : $issue_query->the_post(); ?>
						
					<article class="archive-result <?php echo $post_type; ?>">
						<a class="issue-card" href="<?php the_permalink(); ?>">
							<figure>
								<div class="img-wrap">
									<?php $image = get_field('issue_image'); ?>
									<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
								</div>
								<?php if ( get_field('free_to_watch') ): ?>
									<div class="unlocked"><svg><use xlink:href="#unlocked" /></svg></div>
								<?php else : ?>
									<?php $issueproduct = get_field('issue_product_link'); ?>
									<?php $issueproductid = $issueproduct[0]->ID; ?>
									<?php $issuewooproduct = new WC_Product($issueproductid); ?>
									<?php $issueslug = $issuewooproduct->get_slug(); ?>

									<?php $seriesproduct = get_field('series_product_link'); ?>
									<?php $seriesproductid = $seriesproduct[0]->ID; ?>
									<?php $serieswooproduct = new WC_Product($seriesproductid); ?>
									<?php $seriesslug = $serieswooproduct->get_slug(); ?>

									<?php echo do_shortcode('[wcm_nonmember plans="' . $issueslug . ', ' . $seriesslug . '"]<div class="locked"><svg><use xlink:href="#locked" /></svg></div>[/wcm_nonmember]'); ?>
									<?php echo do_shortcode('[wcm_restrict plans="' . $issueslug . ', ' . $seriesslug . '"]<div class="unlocked"><svg><use xlink:href="#unlocked" /></svg></div>[/wcm_restrict]'); ?>
								<?php endif; ?>
								<div class="button">
									<svg>
										<use xlink:href="#play" />
									</svg>
								</div>
							</figure>
							<div>
								<h6><?php echo $term->name ?> - Issue <?php the_field('issue_number'); ?></h6>
								<h4><?php the_title(); ?></h4>
								<?php if ( get_field('free_to_watch') ): ?>
								<p>Free to watch. No credit card required.</p>
								<?php endif; ?>
							</div>
						</a>
						<button class="activate-share-menu">
							<svg>
								<use xlink:href="#share" />
							</svg>
						</button>
					</article>

					<?php endwhile; ?>
				<?php endif; ?>
			</section>

			<?php } ?>
			<?php wp_reset_postdata(); ?>

		<?php else : ?>

			<article>
				<section class="is-narrow">
					<h2>Coming soon</h2>
				</section>
			</article>

		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>