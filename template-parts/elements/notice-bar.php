<?php 
/*----------------------------------------------------------------*\

	COOKIE USE NOTIFICATION BAR
	cookies used must be cleared via WPengine support

\*----------------------------------------------------------------*/
?>
<?php if ( get_field('site_notice', 'options') ) : ?>
<div class="notice-notification">
	<div> 
		<button> 
			<svg><use xlink:href="#close"></use></svg> 
		</button>
		<p><?php the_field('site_notice', 'options'); ?></p>
	</div>
</div>
<?php endif; ?>