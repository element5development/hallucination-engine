<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a class="logo" href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
		<?php if ( is_user_logged_in() ) { ?>
		<a class="button" href="<?php echo get_home_url(); ?>/my-account/">My Account</a>
		<?php } else { ?>
		<a class="button" href="<?php echo get_home_url(); ?>/my-account/">Register / Login</a>
		<?php } ?>
		<button class="activate-search">
			<svg>
				<use xlink:href="#search" />
			</svg>
		</button>
		<a class="cart" href="<?php echo get_home_url(); ?>/cart/">
			<svg>
				<use xlink:href="#cart" />
			</svg>
		</a>
		<button class="activate-menu">
			<svg>
				<use xlink:href="#menu" />
			</svg>
		</button>
	</nav>
	<div class="search-overlay">
		<?php echo get_search_form(); ?>
		<button class="close">
			<svg>
				<use xlink:href="#close" />
			</svg>
		</button>
	</div>
	<div class="menu-overlay">
		<div class="standard">
			<?php if ( is_user_logged_in() ) { ?>
			<a class="button" href="<?php echo get_home_url(); ?>/my-account/">My Account</a>
			<?php } else { ?>
			<a class="button" href="<?php echo get_home_url(); ?>/my-account/">Register / Login</a>
			<?php } ?>
			<?php wp_nav_menu(array( 'theme_location' => 'secondary_navigation' )); ?>
			<div class="signup-card">
				<div>
					<a class="button" href="<?php echo get_home_url(); ?>/my-account/">Sign Up and Watch Now</a>
				</div>
				<h6>Series 01 - Issue 01</h6>
				<h4>The Great Escape</h4>
				<p>Free to watch. No credit card required.</p>
				<button class="activate-share-menu">
					<svg>
						<use xlink:href="#share" />
					</svg>
				</button>
			</div>
			<button class="close">
				<svg>
					<use xlink:href="#close" />
				</svg>
			</button>
		</div>
	</div>
</div>
<div class="sticky-navigation">
	<?php wp_nav_menu(array( 'theme_location' => 'sticky_navigation' )); ?>
	<button class="activate-search">
		<svg>
			<use xlink:href="#search" />
		</svg>
	</button>
</div>