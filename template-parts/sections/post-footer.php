<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<div class="rating">
			<img class="sgl-rating" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/SGL-Rating.svg" alt="Super Geek League Rating" />
		</div>
		<div class="social">
			<a href="<?php the_field('instagram', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#instagram" />
				</svg>
			</a>
			<a href="<?php the_field('youtube', 'options'); ?>" target="_blank">
				<svg class="youtube">
					<use xlink:href="#youtube" />
				</svg>
			</a>
			<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#facebook" />
				</svg>
			</a>
		</div>
		<a href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo" />
			</svg>
		</a>
		<div class="copyright">
			<p><a href="/privacy-policy/">Privacy Policy</a> <a href="/terms-conditions/">Terms & Conditions</a></p>
			<p>©<?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. A <a href="https://www.supergeekleague.com/" target="_blank">Super Geek League</a> production.</p>
		</div>
		<div id="element5-credit">
			<a target="_blank" href="https://element5digital.com" rel="nofollow">
				<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
			</a>
		</div>
	</div>
</footer> 