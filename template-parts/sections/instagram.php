<?php 
/*----------------------------------------------------------------*\

	INSTAGRAM SECTION

\*----------------------------------------------------------------*/
?>

<section class="instagram is-extra-wide">
	<div>
		<svg>
			<use xlink:href="#instagram"></use>
		</svg>
		<h3>Follow us <span><a href="https://www.instagram.com/thehallucinationengine/" target="_blank">@TheHallucinationEngine</a></span></h3>
	</div>
	<?php echo do_shortcode('[instagram-feed]'); ?>
</section>