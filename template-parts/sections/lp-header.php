<?php 
/*----------------------------------------------------------------*\

	LANDING PAGE HEADER

\*----------------------------------------------------------------*/
?>

<header class="post-head lp-head">
	<div>
		<?php if ( get_field('title') ) : ?>
		<h1><?php the_field('title'); ?></h1>
		<?php else : ?>
		<h1><?php the_title(); ?></h1>
		<?php endif; ?>

		<!-- <div class='youtube-player' data-id='<?php the_field('youtube_id') ?>' data-width='850' data-height='478' data-ssv='false' data-spc='true' data-sta='false' data-afs='true' data-dkc='false' data-ecc='false' data-eap='false'>
			<img src='https://i.ytimg.com/vi/<?php the_field('youtube_id') ?>/maxresdefault.jpg'>
			<svg>
				<use xlink:href="#play-button" />
			</svg>
		</div> -->

		<div class="video-wrap">
			<video controls playsinline poster="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/hallucination-engine-intro-poster.jpg">
				<source src="<?php echo get_stylesheet_directory_uri(); ?>/dist/videos/hallucination-engine-trailer.mp4" type="video/mp4">
				Your browser does not support HTML videos.
			</video>
			<div class="end-overlay">
				<h3>Watch Issue 01 Now:</h3>
				<a class="button is-pink" href="/issue/the-great-escape/">Sign up to watch free</a>
				<p>Or unlock every issue of Series 01:</p>
				<a class="button" href="/cart/?add-to-cart=570">Unlock Series 01 Pass - $24.95</a>
			</div>
		</div>

		<p><?php the_field('description'); ?></p>
		<p class="disclaimer"><?php the_field('disclaimer'); ?></p>
	</div>
</header>