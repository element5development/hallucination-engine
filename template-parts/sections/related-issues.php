<?php 
/*----------------------------------------------------------------*\

	RELATED ISSUES SECTION

\*----------------------------------------------------------------*/
?>
<?php $category = get_the_terms($post->ID, 'series'); ?>
<?php $related = get_posts( array( 'post_type' => 'issue', 'order' => 'ASC', 'category__in' => $category['ID'], 'numberposts' => -1, 'post__not_in' => array($post->ID) ) ); ?>
<?php if( $related ) : ?>
<section class="related-issues issue-grid is-extra-wide">
	<h2>More Issues</h2>
	<?php foreach( $related as $post ) : setup_postdata($post); ?>
		<article class="archive-result <?php echo $post_type; ?>">
			<a class="issue-card" href="<?php the_permalink(); ?>">
				<figure>
					<div class="img-wrap">
						<?php $image = get_field('issue_image'); ?>
						<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					</div>
					<?php if ( get_field('free_to_watch') ): ?>
						<div class="unlocked"><svg><use xlink:href="#unlocked" /></svg></div>
					<?php else : ?>
						<?php $issueproduct = get_field('issue_product_link'); ?>
						<?php $issueproductid = $issueproduct[0]->ID; ?>
						<?php $issuewooproduct = new WC_Product($issueproductid); ?>
						<?php $issueslug = $issuewooproduct->get_slug(); ?>

						<?php $seriesproduct = get_field('series_product_link'); ?>
						<?php $seriesproductid = $seriesproduct[0]->ID; ?>
						<?php $serieswooproduct = new WC_Product($seriesproductid); ?>
						<?php $seriesslug = $serieswooproduct->get_slug(); ?>

						<?php echo do_shortcode('[wcm_nonmember plans="' . $issueslug . ', ' . $seriesslug . '"]<div class="locked"><svg><use xlink:href="#locked" /></svg></div>[/wcm_nonmember]'); ?>
						<?php echo do_shortcode('[wcm_restrict plans="' . $issueslug . ', ' . $seriesslug . '"]<div class="unlocked"><svg><use xlink:href="#unlocked" /></svg></div>[/wcm_restrict]'); ?>
					<?php endif; ?>
					<div class="button">
						<svg>
							<use xlink:href="#play" />
						</svg>
					</div>
				</figure>
				<div>
					<?php foreach ( get_the_terms(get_the_ID(), 'series') as $cat ) : ?>
					<h6><?php echo $cat->name ?> - Issue <?php the_field('issue_number'); ?></h6>
					<?php endforeach; ?>
					<h4><?php the_title(); ?></h4>
					<?php if ( get_field('free_to_watch') ): ?>
					<p>Free to watch. No credit card required.</p>
					<?php endif; ?>
				</div>
			</a>
			<button class="activate-share-menu">
				<svg>
					<use xlink:href="#share" />
				</svg>
			</button>
		</article>
	<?php endforeach; ?>
	<?php wp_reset_postdata(); ?>
</section>
<?php endif; ?>