<?php 
/*----------------------------------------------------------------*\

	NEWSLETTER SECTION

\*----------------------------------------------------------------*/
?>

<section class="newsletter is-wide">
	<div>
		<h3><?php the_sub_field('sign_up_title'); ?></h3>
		<?php echo do_shortcode('[gravityform id=5 title=false description=false ajax=true]'); ?>
	</div>
</section>