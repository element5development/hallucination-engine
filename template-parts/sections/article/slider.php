<?php 
/*----------------------------------------------------------------*\

	SLIDER SECTION

\*----------------------------------------------------------------*/
?>

<section class="slider">
	<?php the_sub_field('content'); ?>
</section>