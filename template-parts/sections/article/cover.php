<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content with background image

\*----------------------------------------------------------------*/
?>
<?php $image = get_sub_field('background'); ?>
<section id="section-<?php echo $template_args['sectionId']; ?>" class="cover <?php the_sub_field('width'); ?> <?php if( !$image ) :?>has-no-image<?php endif; ?>" <?php if( $image ) :?>style="background-image:url('<?php echo $image['sizes']['large']; ?>');"<?php endif; ?>>
	<?php if ( get_sub_field('media') ) : ?>
	<?php $image = get_sub_field('media'); ?>
	<div class="media-content">
		<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 300w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
		<div>
			<?php the_sub_field('content'); ?>
		</div>
	</div>
	<?php else : ?>
	<div>
		<?php the_sub_field('content'); ?>
	</div>
	<?php endif; ?>
</section>