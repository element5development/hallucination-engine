<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying gallery of images

\*----------------------------------------------------------------*/
?>
<section class="gallery is-extra-wide">
	<?php if( have_rows('gallery') ): ?>
		<?php while ( have_rows('gallery') ) : the_row(); ?>

			<div class="gallery-card">
				<?php $image = get_sub_field('image'); ?>
				<figure>
					<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					<a class="button download" download href="<?php echo $image['sizes']['xlarge']; ?>" title="<?php the_sub_field('image_title'); ?>">
						<svg>
							<use xlink:href="#download" />
						</svg>
					</a>
				</figure>
				<div>
					<h4><?php the_sub_field('image_title'); ?></h4>

					<?php $terms = get_sub_field('tags'); ?>
					<?php if( $terms ): ?>
						<?php foreach( $terms as $term ): ?>
							<a href="<?php echo esc_url( get_term_link( $term ) ); ?>">#<?php echo esc_html( $term->name ); ?> </a>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>

		<?php endwhile; ?>
	<?php endif; ?>
</section>