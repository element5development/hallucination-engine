<?php 
/*----------------------------------------------------------------*\

	COUNTDOWN SECTION

\*----------------------------------------------------------------*/
?>

<section class="countdown standard">
	<div>
		<h2>Countdown Until We Launch:</h2>
		<div class="counter">
			<div class="weeks">
				<p>
					<span>##</span>
					weeks
				</p>
			</div>
			<div class="days">
				<p>
					<span>##</span>
					days
				</p>
			</div>
			<div class="hours">
				<p>
					<span>##</span>
					hours
				</p>
			</div>
			<div class="minutes">
				<p>
					<span>##</span>
					minutes
				</p>
			</div>
			<div class="seconds">
				<p>
					<span>##</span>
					seconds
				</p>
			</div>
		</div>
		<div id="get-updates" class="twin-box">
			<h3>Sign up for updates and Watch Issue 1 Free!</h3>
			<p>Sign up to receive email updates on the launch of Hallucination Engine. Issue 1 will be 100% free.</p>
			<?php echo do_shortcode('[gravityform id=5 title=false description=false ajax=true]'); ?>
		</div>
	</div>
</section>