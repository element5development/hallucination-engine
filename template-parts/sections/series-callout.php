<?php 
/*----------------------------------------------------------------*\

	SERIES CALLOUT SECTION

\*----------------------------------------------------------------*/
?>
<section class="series-callout is-narrow">
	<h3>Don't Miss a Single Issue!</h3>
	<div>
		<a class="button" href="<?php echo get_home_url(); ?>/cart/?add-to-cart=570">Unlock the Series Pass - $24.95</a>
	</div>
	<h6>One-time Purchase</h6>
	<h4>Series Pass</h4>
	<p>Buy the Series Pass and unlock access to every issue in the series.</p>
</section>