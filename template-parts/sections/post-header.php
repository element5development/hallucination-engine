<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<header class="post-head is-wide">
	<h1><?php the_title(); ?></h1>
</header>