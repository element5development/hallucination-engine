<?php 
/*----------------------------------------------------------------*\

	SINGLE POST FOR CPT: BONKAPEDIA

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main id="main-content">
	<article class="is-wide">
		<?php if (have_posts()) : ?>
			<?php	while ( have_posts() ) : the_post(); ?>
				<?php $image = get_field('image'); ?>
				<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
				
				<div class="standard">
					<div class="title-wrap">
						<?php $next_post = get_next_post(true, '', 'bonk_category'); ?>

						<?php if( !empty( $next_post ) ) : ?>
								<?php next_post_link('%link', "<svg><use xlink:href='#arrow' /></svg>", true, '', 'bonk_category'); ?>
						<?php endif; ?>

						<?php if ( get_field('full_name') ) : ?>
						<h1><?php the_field('full_name'); ?></h1>
						<?php else : ?>
						<h1><?php the_title(); ?></h1>
						<?php endif; ?>

						<?php $prev_post = get_previous_post(true, '', 'bonk_category'); ?>

						<?php if( !empty( $prev_post ) ) : ?>
							<?php previous_post_link('%link', "<svg><use xlink:href='#arrow' /></svg>", true, '', 'bonk_category'); ?>
						<?php endif; ?>
					</div>

					<?php the_content(); ?>
					<button class="activate-share-menu">
						<svg>
							<use xlink:href="#share" />
						</svg>
						Share
					</button>

					<?php $posts = get_field('merch'); ?>
					<?php if( $posts ): ?>
						<h4>Merch</h4>
						<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
								<?php setup_postdata($post); ?>

								<a class="product-card" href="<?php the_permalink(); ?>">
									<?php 
										global $post;
										$product = new WC_Product($post->ID); 
										$price = wc_price($product->get_price_excluding_tax(1,$product->get_price()));
									?>

									<?php echo get_the_post_thumbnail( $p->ID ); ?>
									<?php the_title(); ?> - <?php echo $price; ?>
								</a>

						<?php endforeach; ?>
						<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>

					<?php if( have_rows('downloads') ): ?>
						<h4>Downloads</h4>
						<?php while ( have_rows('downloads') ) : the_row(); ?>
							<?php
								$link = get_sub_field('download'); 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self'; 
							?>
							<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						<?php endwhile; ?>
					<?php endif; ?>

					<h4>Tags</h4>
					<?php foreach ( get_the_terms(get_the_ID(), 'bonk_tag') as $tag ) : ?>
					<a class="tag" href="/bonk_tag/<?php echo $tag->slug ?>/">#<?php echo $tag->name ?></a>
					<?php endforeach; ?>

					<?php if ( is_single('543') ) { ?>
						<a class="the-cult" href="/league-of-bonk/">
							<svg>
								<use xlink:href="#flower" />
							</svg>
						</a>
					<?php } ?>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>