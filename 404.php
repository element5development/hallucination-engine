<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main id="main-content">
	<article>
		<section class="is-narrow aligncenter">
			<h1>404</h1>
			<p>page not found…</p>
		</section>
	</article>
</main>

<?php get_footer(); ?>