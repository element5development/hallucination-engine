<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<h2>
	<?php
	printf(
		esc_html( $current_user->display_name ),
	);
	?>
</h2>

<?php $points_balance = WC_Points_Rewards_Manager::get_users_points( get_current_user_id() ); ?>
<p class="points"><strong><?php echo $points_balance; ?></strong> Bonk Points <a href="/faqs/#bonk">(Learn More)</a></p>
<h4>Download the App!</h4>
<p>You can save Hallucination Engine to your mobile device as an app for faster loading. Just choose ‘Add to Home Screen’ on compatible devices.</p>

<section class="issue-grid">
	<h3>My Issues</h3>
	<?php 
		$post_args = array ( 
			'posts_per_page'	=> -1, 
			'post_type'				=> 'issue',
			'order'						=> 'ASC',
		);
		$issue_query = new WP_Query( $post_args );
	?>
	<?php if ( $issue_query->have_posts() ) : ?>
		<?php while ( $issue_query->have_posts() ) : $issue_query->the_post(); ?>
			
		<?php $issueproduct = get_field('issue_product_link'); ?>
		<?php $issueproductid = $issueproduct[0]->ID; ?>
		<?php $issuewooproduct = new WC_Product($issueproductid); ?>
		<?php $issueslug = $issuewooproduct->get_slug(); ?>

		<?php $seriesproduct = get_field('series_product_link'); ?>
		<?php $seriesproductid = $seriesproduct[0]->ID; ?>
		<?php $serieswooproduct = new WC_Product($seriesproductid); ?>
		<?php $seriesslug = $serieswooproduct->get_slug(); ?>

		<article class="archive-result issue <?php if ( get_field('free_to_watch') ): ?>is-free<?php endif; ?><?php echo do_shortcode('[wcm_nonmember plans="' . $issueslug . ', ' . $seriesslug . '"]not-purchased[/wcm_nonmember]'); ?>">
			<a class="issue-card" href="<?php the_permalink(); ?>">
				<figure>
					<div class="img-wrap">
						<?php $image = get_field('issue_image'); ?>
						<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					</div>
					<?php if ( get_field('free_to_watch') ): ?>
						<div class="unlocked"><svg><use xlink:href="#unlocked" /></svg></div>
					<?php else : ?>
						<?php echo do_shortcode('[wcm_nonmember plans="' . $issueslug . ', ' . $seriesslug . '"]<div class="locked"><svg><use xlink:href="#locked" /></svg></div>[/wcm_nonmember]'); ?>
						<?php echo do_shortcode('[wcm_restrict plans="' . $issueslug . ', ' . $seriesslug . '"]<div class="unlocked"><svg><use xlink:href="#unlocked" /></svg></div>[/wcm_restrict]'); ?>
					<?php endif; ?>
					<div class="button">
						<svg>
							<use xlink:href="#play" />
						</svg>
					</div>
				</figure>
				<div>
					<?php foreach ( get_the_terms(get_the_ID(), 'series') as $cat ) : ?>
					<h6><?php echo $cat->name ?> - Issue <?php the_field('issue_number'); ?></h6>
					<?php endforeach; ?>
					<h4><?php the_title(); ?></h4>
					<?php if ( get_field('free_to_watch') ): ?>
					<p>Free to watch. No credit card required.</p>
					<?php endif; ?>
				</div>
			</a>
			<button class="activate-share-menu">
				<svg>
					<use xlink:href="#share" />
				</svg>
			</button>
		</article>

		<?php endwhile; ?>
	<?php endif; ?>
</section>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */