<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main id="main-content">
	<?php if ( get_field('message') ) : ?>
		<article>
			<section class="is-narrow aligncenter">
				<h1><?php the_title(); ?></h1>
				<p><?php the_field('message'); ?></p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_footer(); ?>