<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>

	<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ece97560c6b07001257cdee&product=inline-share-buttons" async="async"></script>

	<?php //general stuff ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name="google-site-verification" content="CUoQPoXBjP5kCISYzG2UljJXyFOFPh9FeqeNaayLYO4" />
	<?php
	/*----------------------------------------------------------------*\
	|
	|	title, social and other seo tags are all handled and inserted 
	| via The SEO Framework which is a must have plugin
	|
	\*----------------------------------------------------------------*/
	?>
	<?php // force Internet Explorer to use the latest rendering engine available ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<?php wp_head(); ?>
	
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P6DQZPT');</script>
	<!-- End Google Tag Manager -->

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '2662217190544491'); 
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" src="https://www.facebook.com/tr?id=2662217190544491&ev=PageView&noscript=1"/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Facebook Add to Cart Event -->
	<!-- 489 is Cart -->
	<?php if( is_page(489) ) { ?>
		<script>fbq('track', 'AddToCart');</script>
	<?php } ?>
	<!-- End Facebook Add to Cart Event -->

	<!-- Facebook Initiate Checkout Event -->
	<!-- 490 is Checkout -->
	<?php if( is_page(490) ) { ?>
		<script>fbq('track', 'InitiateCheckout');</script>
	<?php } ?>
	<!-- End Facebook Initiate Checkout Event -->

	<!-- Facebook Complete Purchase Event -->
	<?php if( is_checkout() && is_wc_endpoint_url('order-received') ) { ?>
	<?php 
		$order_id = wc_get_order_id_by_order_key( $_GET['key'] );
		$order = wc_get_order( $order_id );
	?>
		<?php if ( $order ) : ?>
			<?php 
				$total = $order->get_total();
				$orderid = $order->get_order_number();
			?>
		<?php else : ?>
			<?php 
				$total = "0.00";
				$orderid	= "0";
			?>
		<?php endif; ?>
	<script>fbq('track', 'Purchase', {value: <?php echo $total ?>, currency: 'USD', order_id: '<?php echo $orderid ?>'});</script>
	<?php } ?>
	<!-- End Facebook Complete Purchase Event -->
</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P6DQZPT"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<a id="skip-to-content" href="#main-content">Skip to main content</a>

	<?php get_template_part('template-parts/icon-set'); ?>

	<?php get_template_part('template-parts/elements/notice-bar'); ?>

	<?php get_template_part('template-parts/elements/cookie-bar'); ?>

	<?php get_template_part('template-parts/elements/navigation'); ?>

	<div class="share-overlay">
		<div>
			<h3>Share this with friends!</h3>
			<div class="sharethis-inline-share-buttons"></div>
			<button class="close">
				<svg>
					<use xlink:href="#close" />
				</svg>
			</button>
		</div>
	</div>