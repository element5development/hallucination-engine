<?php 
/*----------------------------------------------------------------*\

	HTML FOOTER CONTENT
	Commonly only used to close off open containers
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php wp_footer(); ?>

<?php
	$time = get_field('launch_time', 'option');
	$date = get_field('launch_day', 'option', false, false);
	$date = new DateTime($date);
	// echo $date->format('Y/m/d'); echo $time; 
?>
<script type="text/javascript"> // Get the Countdown Values
	jQuery('.countdown').countdown('<?php echo $date->format('Y/m/d'); ?> <?php echo $time; ?>', function(event) {
		jQuery('.weeks p span').html(event.strftime('%w'));
		jQuery('.days p span').html(event.strftime('%d'));  
		jQuery('.hours p span').html(event.strftime('%H'));
		jQuery('.minutes p span').html(event.strftime('%M'));
		jQuery('.seconds p span').html(event.strftime('%S'));
	});
</script>

</body>

</html>