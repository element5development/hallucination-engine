<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<header class="post-head is-wide">
	<h1><?php echo 'Search results for: ' . get_search_query(); ?></h1>
</header>

<main id="main-content">
	<?php if (have_posts()) : ?>
		<?php	while ( have_posts() ) : the_post(); ?>
			<article class="search-result is-narrow <?php echo get_post_type(); ?>">
				<header>
					<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
				</header>
				<section class="entry-content">
					<?php the_excerpt(); ?>
					<a class="button" href="<?php the_permalink(); ?>">Continue Reading</a>
				</section>
			</article>
		<?php endwhile; ?>
	<?php else : ?>
		<article>
			<section class="is-narrow">
				<p>We cannot find anything for "<?php echo(get_search_query()); ?>".</p>
			</section>
		</article>
	<?php endif; ?>
	<?php clean_pagination(); ?>
</main>

<?php get_footer(); ?>