var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
  	ACTIVATE SEARCH
	\*----------------------------------------------------------------*/
	$("button.activate-search").click(function () {
		$(".search-overlay").addClass("is-active");
		$(".search-overlay form input").focus();
		$(".menu-overlay").removeClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$("button.activate-menu").click(function () {
		$(".menu-overlay").addClass("is-active");
		$(".search-overlay").removeClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE SHARE MENU
	\*----------------------------------------------------------------*/
	$("button.activate-share-menu").click(function () {
		$(".share-overlay").addClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	CLOSE OVERLAYS
	\*----------------------------------------------------------------*/
	$("button.close").click(function () {
		$(".search-overlay").removeClass("is-active");
		$(".menu-overlay").removeClass("is-active");
		$(".share-overlay").removeClass("is-active");
	});
	/*----------------------------------------------------------------*\
		NOTIFICATION BARS
	\*----------------------------------------------------------------*/
	if (readCookie('noticeNotification') === 'false') {
		$('.notice-notification').removeClass("note-on");
	} else {
		$('.notice-notification').addClass("note-on");
	}
	$('.notice-notification button').click(function () {
		$('.notice-notification').removeClass("note-on");
		createCookie('noticeNotification', 'false');
	});

	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}
	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		LAZYLOAD CLASS FOR IFRAMES
	\*----------------------------------------------------------------*/
	$(function () {
		$('iframe').addClass('lazyload');
	});

	/*----------------------------------------------------------------*\
		GALLERY SLIDER
	\*----------------------------------------------------------------*/
	$('.gallery.slider ul').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: false,
		prevArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M7.92 0l-6 6 6 6 2.1-2.1L6.12 6l3.9-3.9L7.92 0z"/></svg></button>',
		nextArrow: '<button class="is-white"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><path d="M1.98 2.1L5.88 6l-3.9 3.9 2.1 2.1 6-6-6-6-2.1 2.1z"/></svg></button>',
		responsive: [{
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	/*----------------------------------------------------------------*\
		SKIP INTRO BUTTON
	\*----------------------------------------------------------------*/
	if ($('body.single-issue').length > 0) {
		var vid = $('video')[0];
		$('#skip-intro').click(function(){
			skipTime(20);
			$(this).removeClass('is-visible');
		});

		function skipTime(time) {
			vid.play();
			vid.pause();
			vid.currentTime = time;
			vid.play();
		};

		vid.addEventListener("timeupdate", function(){
			if(vid.currentTime > 3) {
				$('#skip-intro').addClass('is-visible');
			}
			if(vid.currentTime >= 10) {
				$('#skip-intro').removeClass('is-visible');
			}
		});
	}
	/*----------------------------------------------------------------*\
		ON VIDEO PLAY GO FULLSCREEN
	\*----------------------------------------------------------------*/
	$('video').on('play',function(){
		if (this.requestFullscreen) {
			this.requestFullscreen();
		} else if (this.mozRequestFullScreen) { /* Firefox */
			this.mozRequestFullScreen();
		} else if (this.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
			this.webkitRequestFullscreen();
		} else if (this.msRequestFullscreen) { /* IE/Edge */
			this.msRequestFullscreen();
		}
	});
	/*----------------------------------------------------------------*\
		VIDEO COMPLETE EVENT
	\*----------------------------------------------------------------*/
	$('video').on('ended',function(){
		if (this.exitFullscreen) {
			this.exitFullscreen();
		} else if (this.mozCancelFullScreen) { /* Firefox */
			this.mozCancelFullScreen();
		} else if (this.webkitExitFullscreen) { /* Chrome, Safari and Opera */
			this.webkitExitFullscreen();
		} else if (this.msExitFullscreen) { /* IE/Edge */
			this.msExitFullscreen();
		}
		$('.end-overlay').addClass('is-active');
	});
	/*----------------------------------------------------------------*\
		REDIRECT TO LANDING PAGE ON ISSUE 01 COMPLETION
	\*----------------------------------------------------------------*/
	if ($('body.single-issue.postid-544.access-restricted').length > 0) {
		$('video').on('ended',function(){
			window.location.replace("https://hallucinationengine.com/whats-next/");
		});
	}
});