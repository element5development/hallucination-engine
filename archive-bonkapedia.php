<?php 
/*----------------------------------------------------------------*\

	ARCHIVE FOR CPT: BONKAPEDIA

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<main id="main-content" class="is-extra-wide">
	<aside>
		<h1><?php the_field($post_type.'_title','options'); ?></h1>
		<p><?php the_field('bonkapedia_description','options'); ?></p>

		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<label for="s">Search or use the categories below:</label>
			<input type="search" id="s" name="s" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" />
			<input name="post_type" type="hidden" value="bonkapedia" />
			<button type="submit">Search</button>
		</form>

		<?php $terms = get_terms( 'bonk_category' ); ?>
		<?php foreach ( $terms as $term ) { ?>
			<a href="/bonk_category/<?php echo $term->slug ?>/"><h4><?php echo $term->name ?></h4></a>
		<?php } ?>
		<?php wp_reset_postdata(); ?>
	</aside>
	<article>
		<?php if (have_posts()) : ?>
			<section class="featured-post">
				<a href="<?php the_permalink(); ?>">
					<h2>Featured: <?php the_title(); ?></h2>
					<?php $image = get_field('image'); ?>
					<img class="lazyload blur-up" data-expand="500" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['large']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
					<?php foreach ( get_the_terms(get_the_ID(), 'bonk_category') as $cat ) : ?>
					<h6><?php echo $cat->name ?></h6>
					<?php endforeach; ?>

					<h3><?php the_title(); ?></h3>
				</a>

				<?php foreach ( get_the_terms(get_the_ID(), 'bonk_tag') as $tag ) : ?>
				<a class="tag" href="/bonk_tag/<?php echo $tag->slug ?>/">#<?php echo $tag->name ?></a>
				<?php endforeach; ?>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>